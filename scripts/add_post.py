import requests
import json

post_length = 30

lorem = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy"


for i in range(post_length):
    response = requests.post('http://localhost:8000/api/posts', data=json.dumps({
        "title": "This is post {0}".format(i+1),
        "description": "This is post desc {0} {1}".format(i+1, lorem)
    }), headers={
        'Content-Type': 'application/json'
    })

    print(response.text)
    print(response.status_code)
