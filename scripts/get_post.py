import requests
import json

response = requests.get('http://localhost:8000/api/post/2', headers={
    'Content-Type': 'application/json'
})
print(response.text)
print(json.dumps(json.loads(response.text), indent=2))
