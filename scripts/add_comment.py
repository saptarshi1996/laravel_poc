import requests
import json


response = requests.post('http://localhost:8000/api/comments/2', data=json.dumps({
    "title": "This is comment {0} on post 2".format(1)
}), headers={
    'Content-Type': 'application/json'
})

print(response.text)
print(response.status_code)
