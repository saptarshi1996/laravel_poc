<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\CommentController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Post Routes
Route::get('/posts', [PostController::class, 'getAllPostsPaginated']);

Route::get('/posts_all', [PostController::class, 'getAllPosts']);

Route::post('/posts', [PostController::class, 'addPost']);

Route::get('/post/{id}', [PostController::class, 'getPost']);
Route::put('/post/{id}', [PostController::class, 'updatePost']);
Route::delete('/post/{id}', [PostController::class, 'deletePost']);


// Comment Routes
Route::post('/comments/{id}', [CommentController::class, 'addComment']);
Route::get('/comments/{id}', [CommentController::class, 'getCommentsByPostId']);
Route::delete('/comment/{id}', [CommentController::class, 'deleteComment']);
