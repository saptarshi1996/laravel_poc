<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Post;
use App\Models\Comment;

class PostController extends Controller
{

    public function getAllPosts()
    {
        $post = Post::get()->toJson();
        return response($post, 200);
    }

    public function getAllPostsPaginated()
    {
        return Post::paginate();
    }

    public function addPost(Request $request)
    {
        try {

            $post = new Post;
            $post->title = $request->title;
            $post->description = $request->description;

            $post->save();

            return response()->json([
                "message" => "Post saved succesfully"
            ], 201);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function getPost($id)
    {
        try {
            if (Post::where('id', $id)->exists()) {
                $post = Post::with('comments')->find($id)->toJson();
                return response($post, 200);
            } else {
                return response()->json([
                    "message" => "Post does not exists"
                ], 404);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function updatePost(Request $request, $id)
    {
        try {
            if (Post::where('id', $id)->exists()) {

                $post = Post::find('id', $id);

                $post->title = is_null($request->title) ? $request->title : $post->title;
                $post->description = is_null($request->description) ? $request->description : $post->description;
                $post->save();
            } else {
                return response()->json([
                    "message" => "Post not found"
                ], 404);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function deletePost($id)
    {
        try {
            if (Post::where('id', $id)->exists()) {

                // delete all the related comments.
                Comment::where('post_id', $id)->delete();
                // delete the post.
                Post::find($id)->delete();

                return response()->json([
                    "message" => "Post deleted"
                ], 202);
            } else {
                return response()->json([
                    "message" => "Post not found"
                ], 404);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }
    }
}
