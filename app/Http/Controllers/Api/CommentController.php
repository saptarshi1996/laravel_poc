<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Post;
use App\Models\Comment;

class CommentController extends Controller
{

    public function addComment(Request $request, $id)
    {
        try {
            if (Post::where('id', $id)->exists()) {
                try {

                    $comment = new Comment;
                    $comment->title = $request->title;
                    $comment->post_id = $id;
                    $comment->save();

                    return response()->json([
                        "message" => "Comment saved successfully"
                    ], 201);
                } catch (\Exception $exception) {
                    return response()->json([
                        "message" => $exception->getMessage()
                    ], 500);
                }
            } else {
                return response()->json([
                    "message" => "Post not found"
                ], 200);
            }
        } catch (\Exception $exception) {
            return response()->json([
                "message" => $exception->getMessage()
            ], 500);
        }
    }

    public function getCommentsByPostId($id)
    {
        try {
            if (Post::where('id', $id)->exists()) {
                return Comment::where('post_id', $id)::paginate();
            } else {
                return response()->json([
                    "message" => "Post not found"
                ], 404);
            }
        } catch (\Exception $exception) {
            return response()->json([
                "message" => $exception->getMessage()
            ], 500);
        }
    }

    public function deleteComment($id)
    {
        try {
            if (Comment::where('id', $id)->exists()) {
                Comment::find($id)->delete();
                return response()->json([
                    "message" => "Comment deleted"
                ], 201);
            } else {
                return response()->json([
                    "message" => "Post not found"
                ], 404);
            }
        } catch (\Exception $exception) {
            return response()->json([
                "message" => $exception->getMessage()
            ], 500);
        }
    }
}
